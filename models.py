from django.db import models

class numdb(models.Model):
    id = models.IntegerField(primary_key=True)
    num = models.CharField(max_length=7)
    name = models.CharField(max_length=256)
    addr = models.CharField(max_length=256)
    mgst = models.CharField(max_length=128)
    rspr = models.CharField(max_length=256)
    shkf = models.CharField(max_length=128)
    data = models.CharField(max_length=128)
    prhd = models.CharField(max_length=256)
    note = models.CharField(max_length=256)
    rsrv = models.CharField(max_length=128)
    indx = models.IntegerField()
    intf = models.CharField(max_length=2)
    l3 = models.CharField(max_length=4)
    plac = models.CharField(max_length=6)
    plnt = models.CharField(max_length=2)
    para = models.CharField(max_length=2)
    free = models.CharField(max_length=1)
    dgzd = models.CharField(max_length=1)
    class Meta:
            db_table = u'numdb'

class jbill(models.Model):
    id = models.IntegerField(primary_key=True)
    numa = models.CharField(max_length=20)
    numb = models.CharField(max_length=20)
    date = models.DateTimeField()
    drtn = models.SmallIntegerField()
    src = models.SmallIntegerField()
    class Meta:
            db_table = u'jbill'

class histo(models.Model):
    id = models.IntegerField(primary_key=True)
    num = models.CharField(max_length=7)
    type = models.CharField(max_length=7)
    date = models.DateTimeField()
    xold = models.CharField(max_length=64)
    xnew = models.CharField(max_length=64)
    class Meta:
        db_table = u'histo'

class ndxdb(models.Model):
    indx = models.IntegerField(primary_key=True)
    ryad = models.CharField(max_length=2)
    blck = models.CharField(max_length=2)
    plnt = models.CharField(max_length=2)
    para = models.CharField(max_length=2)
    class Meta:
        db_table = u'ndxdb'
