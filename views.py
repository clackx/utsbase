# -*- encoding: UTF-8 -*-
from django.shortcuts import render_to_response, redirect
from models import numdb, jbill, histo, ndxdb
from datetime import datetime, timedelta, date
import Image, ImageDraw, ImageFont  #for create images
from os import path     
from time import mktime
from django.forms.models import model_to_dict

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

class A(object):
    def __init__(self):
        self.b = 1
        self.c = 2
    def do_nothing(self):
        pass

def list(request):
    timestart = datetime.now()
    
    req = '63042'
    #print request.GET
    #if request.GET == {}:
    #    request.GET = {u'num': [u'6']}
    #print request.GET

    nmreq = '6'
    #print request

    if request.GET:
        if request.GET['num'] <> '':
            req = request.GET['num'].encode('utf8', 'ignore')
            nmreq = req 
            if req[-1:] == '.':
                db = numdb.objects.order_by('num').filter(num__iregex = '^'+req[:-1]).using('db')
            else:
                db = numdb.objects.order_by('num').filter(num__iregex = req).using('db')
        elif request.GET['name'] <> '':
            req = request.GET['name'].encode('utf8', 'ignore')
            rs=req.split(" ")
            if len (rs) == 1:
                db = numdb.objects.order_by('num').filter(name__icontains = req).using('db')
            else:
                db = numdb.objects.order_by('rspr').filter(name__icontains = rs[0]).filter(name__icontains = rs[1]).using('db')

        elif request.GET['addr'] <> '':
            req = request.GET['addr'].encode('utf8', 'ignore')
            rs=req.split(" ")
            if len (rs) == 1:
                db = numdb.objects.order_by('rspr').filter(addr__icontains = req).using('db')
            elif len (rs) == 2:
                #req = "r'(" + str( '|'.join(rs))[0:] +  ")'"
                db = numdb.objects.order_by('rspr').filter(addr__icontains = rs[0]).filter(addr__icontains = rs[1]).using('db')
            elif len (rs) == 3:
                db = numdb.objects.order_by('rspr').filter(addr__icontains = rs[0]).filter(addr__icontains = rs[1]).filter(addr__icontains = rs[2]).using('db')
            else:
                db = numdb.objects.order_by('rspr').filter(addr__icontains = rs[0]).filter(addr__icontains = rs[1]).filter(addr__icontains = rs[2]).filter(addr__icontains = rs[3]).using('db')
        elif request.GET['mgst'] <> '':
            req = request.GET['mgst'].encode('utf8', 'ignore')
            regexp = ''
            for i in range(len(req)):
                if req[i] == '*':
                    regexp += '[0-9]'
                else:
                    regexp += req[i]
            if req[-1:] == '.':
                db = numdb.objects.order_by('mgst').filter(mgst = req[:-1]).using('db')
            else:
                db = numdb.objects.order_by('mgst').filter(mgst__iregex = r'^' + regexp + '$' + '$'  ).using('db')
        elif request.GET['rspr'] <> '':
            req = request.GET['rspr'].encode('utf8', 'ignore')
            if req[-1:] == '.':
                db = numdb.objects.order_by('rspr').filter(rspr = req[:-1]).using('db')
            else:
                db = numdb.objects.order_by('rspr').filter(rspr__iregex = req).using('db')
    else:
            db = numdb.objects.order_by('num').filter(num__iregex = '63.').using('db')

    print 'Запрос: "' + req + '"'


    if request.POST:
        req = request.POST
        print req
        #db = numdb.objects.order_by('num').filter(num__iregex = '6').using('db')
        if 'srch' in req:
            srch = req['srch']
            print '>>>>>>>>', srch
        if 'mgst' in req:
            db = numdb.objects.order_by('mgst').filter(mgst__iregex = srch).using('db')
            print db
    
    if len(db) == 0:
        print "Ничего не найдено :("
    #print '>>>', db
    
    timefinish = datetime.now()
    
    return render_to_response('list.html', {
            'jb_filt': db, 'len' : len (db),  'tim': timefinish - timestart,
            'req' : nmreq,
            #'jb_cols': k, 'jb_time': time,
            #'jb_locl': locl, 'jb_intr': intr, 'jb_num': num,
    })


def srch(request):
    timestart = datetime.now()
   
    #db = numdb.objects.order_by('num').filter(num__iregex = '6').using('db')

    timefinish = datetime.now()

    return render_to_response('srch.html', {
            #'jb_filt': db, 'len' : len (db),
            'tim': timefinish - timestart,
            #'req' : "6.", #'jb_cols': k, 'jb_time': time,
            #'jb_locl': locl, 'jb_intr': intr, 'jb_num': num,
    })



def edit(request):
    timestart = datetime.now()
   
    num = request.path.split('/')[2]
    if num == '':
        num = '63042'

    dba = numdb.objects.filter(num = num).using('db')

    db = dba[0].__dict__
    del db['_state']

    print db['indx']
    if db['indx']:
        dbi = ndxdb.objects.filter( indx = db['indx'] ).using('db')[0]
        db['plac'] = dbi.ryad + '/' + dbi.blck 
        db['plnt'] = dbi.plnt
        db['para'] = dbi.para

    hdb = dict.fromkeys(db)
    #print hdb

    dbhist = histo.objects.filter(num = num).using ('db')
    for i in range(0,len(dbhist)):
        hdb[dbhist[i].type] = dbhist[i].date

    print datetime.now(), "Просмотр номера", num
    #print hdb

    if request.POST:
        rq = request.REQUEST
        k = rq.keys()
        
        for i in range(len(k)):
            iold = db[ k[i] ]
            inew = rq[ k[i] ]
            #print '>>>>>>>', inew
            if isinstance(iold,int):
                iold = str(iold)
            if iold == '' and k[i] == 'dgzd':
                iold = '0'
            #else:
                #print '>>', iold, "<<"
            #print k[i], '::\t',  iold, '\t\t/', inew
            if iold <> inew:
                #iold = iold.encode('utf8', 'ignore')
                #inew = iold.encode('utf8', 'ignore')
                print iold, inew
                dtnow = datetime.now()
                hidb = histo (num = str(num), type = k[i], date = dtnow, xold = iold, xnew = inew)
                hdb[k[i]] = dtnow
                print "Изменение поля", k[i], "старое значение: '" + str(iold.encode('utf8', 'ignore')) + "', новое: '" + str(inew.encode('utf8', 'ignore')) + "'"
                hidb.save(using = 'db')

                db[ k[i] ] = inew
                hda = numdb (**db)
                #print '!!!!!!!!!!!', hda.name, hda.dgzd, hda.free
                hda.save(using = 'db')

        
    timefinish = datetime.now()
     
    return render_to_response('edit.html', {
            'q': db,  'h': hdb,
            'tim': timefinish - timestart,
            'num': num, 'prev': int(num)-1, 'next':int(num)+1,
            #'jb_locl': locl, 'jb_intr': intr, 'jb_num': num,
    })


def free(request):
    timestart = datetime.now()

    req = request.GET['num'].encode('utf8', 'ignore')
    nmreq = req
    if req[-1:] == '.':
        db = numdb.objects.order_by('num').filter(num__iregex = '^'+req[:-1]).using('db')
    else:
        db = numdb.objects.order_by('num').filter(num__iregex = req).using('db')

    dbarr = [] 
    for d in db:
        dbarr.append(d.num)

    prdk = 10**(6-len(req))
    pool =  int(req[:-1])*prdk
    print pool,pool+prdk

    if pool == 60000:
        pool=62000
        prdk=5600
    
    if pool == 67000:
        prdk = 600

    #numnew = numdb(num = '63225', free=0)
    #numnew.save(using='db')
    #numdb.objects.order_by('num').filter(num__iregex = '^'+req[:-1]).using('db')

    #print set(dbarr)
    #print set(str(range(62000,63000))) #- set(dbarr)
    narr = set(range(pool,pool+prdk))
    nsarr = []
    for i in narr:
        nsarr.append(str(i))
    #print nsarr

    #q2 = set(nsarr) - set(dbarr)
    q0 = numdb.objects.order_by('num').filter(free = 1).using('db')
    q2 = numdb.objects.order_by('num').filter(prhd = 'duplicate!').using('db')
    #print 
    q1 = set(dbarr) - set(nsarr)

    #insert free numbrs
    #for q in q0:
        #numnew = numdb(num = str(q), free=1)
        #numnew.save(using='db')

    timefinish = datetime.now()
    return render_to_response('free.html', {
            'q': q0, 
            'q1': sorted(q1),
            'q2': q2,
            'tim': timefinish - timestart #'jb_cols': k, 'jb_time': time,
            #'jb_locl': locl, 'jb_intr': intr, 'jb_num': num,
    }) 


def bill(request):
    timestart = datetime.now()

    inplst = []
    mssg =  'Введите пул номеров'

    if request.POST:
        inplst = request.POST['info'].split(' ')
        print 'Размерность: ', len(inplst)
        #print inplst
        ind = 0

    #q0 = jbill.objects.order_by('numa').filter(numa = inplst[0]).using('db')
    #q2 = jbill.objects.order_by('numb').filter(numb = inplst[1]).using('db')
    #print q0[0].numb 

    mssg = ''
    k8t = 0; d8t = 0
    k0t = 0; d0t = 0
    for num in inplst:
        if num[:3] == '216':
            num = num[2:]
        #print 
        calls = jbill.objects.order_by('numa').filter(numa = num, date__gte = '2012-12-01 00:00:00').using('db')
        k8 = 0; d8 = 0
        k0 = 0; d0 = 0
        plus = True
        if len(calls) > 0:
            for call in calls:
                #k += 1
                if len(call.numb) > 5:
                    if call.numb[:3] == '849':
                        k8 += 1; d8 += call.drtn
                        mssg = mssg + str(call.numa) +  '\t 8[РТ] ' + str(call.numb) + '\t' + str(call.date) + '\t' +  str(call.drtn) +'\n'
                    elif call.numb[:3] == '049':
                        k8 += 1; d8 += call.drtn
                        mssg = mssg + str(call.numa) + '\t 0[РТ] 8' + str(call.numb[1:]) + '\t' + str(call.date) +  '\t' + str(call.drtn) + '\n'
                    elif call.numb[:4] == '0049':
                        k0 += 1; d0 += call.drtn
                        mssg = mssg + str(call.numa) + '\t *[СЭ] 8' + str(call.numb[2:]) + '\t' + str(call.date) + '\t' + str(call.drtn) + '\n'
                    #elif call.numb[:3] == '649':
                    #    k8 += 1; d8 += call.drtn
                    #    mssg = mssg + str(call.numa) + '\t [649] 8' + str(call.numb[1:]) + '\t' + str(call.date) +  '\t' + str(call.drtn) + '\n'
                    elif call.numb[:2] == '49':
                        k8 += 1; d8 += call.drtn
                        mssg = mssg + str(call.numa) + '\t 4[РТ] 8' + str(call.numb) + '\t' + str(call.date) +  '\t' + str(call.drtn) + '\n'
                    elif call.numb.find('49') > -1 and call.numb.find('49') < 5:
                        print str(call.numa) + '\t' + str(call.numb) + '\t' + str(call.date) +  '\t' + str(call.drtn) + '\n'
                else:
                    plus = False
                #d += call.drtn
                #print call.numa, call.date, call.drtn
            mssg = mssg + '::' + str(call.numa) + '::\t [РТ] ' + str (k8) + 'шт. ' + str(d8) + 'сек.,\t[CЭ] ' + str(k0) + 'шт. ' + str(d0) +  'сек.\n\n'
            k8t += k8; k0t += k0; d8t += d8; d0t += d0

    mssg = mssg + 'Итого :: \t [РТ] ' + str (k8t) + 'шт. ' + str(d8t) + 'сек.,\t[CЭ] ' + str(k0t) + 'шт. ' + str(d0t) +  'сек.\n\n'

    timefinish = datetime.now()

    return render_to_response('bill.html', {
            #'jb_filt': db, 'len' : len (db),  
            'tim': timefinish - timestart,
            'default': mssg,
            #'jb_cols': k, 'jb_time': time,
            #'jb_locl': locl, 'jb_intr': intr, 'jb_num': num,
    })

def add(request):
    timestart = datetime.now()
   
    timefinish = datetime.now()

    return render_to_response('edit.html', {
            #'jb_filt': db, 'len' : len (db),  
            'tim': timefinish - timestart
    })


def hstr(request):
    timestart = datetime.now()

    alll = histo.objects.all().using('db')
    print alll[0].num, len(alll)

    timefinish = datetime.now()

    return render_to_response('hstr.html', {
            #'jb_filt': db, 'len' : len (db), 
            'db' : alll, 
            'tim': timefinish - timestart
    })



def iupd(request):
    timestart = datetime.now()

    #f = open('./img/index.csv')
    #if 

    inplst = []

    if request.POST:
        inplst = request.POST['info'].split(' ')
        print 'Размерность: ', len(inplst)
        #print inplst
        ind = 0
    
        if len(inplst) == 3841:
            inplst = inplst[:-1]
        
        if len(inplst) <> 3840:
            mssg = 'Список должен содержать 3840 записей. Если Вы уверены, что всё сделали правильно, обратитесь к разработчику.'
        else:
            mssg = 'Внесённые изменения:\n'
            for num in inplst:
                #print ind
                ind += 1
        
                if num[:3] == '216':
                    num = num[2:]
                    numq = numdb.objects.filter(num = num).using('db')
                    for i in range(0,len(numq)):
                        iold = numq[0].indx
                        if not iold:
                            iold = 0
                        inew = ind
                        #numq[i].dgzd = 0
                        #numq[i].save()
                        #print num, numq[i]
                        #print '::', iold, '::', inew, '::', iold <> inew
                        if iold <> inew:
                            mssg = mssg +  str(num) + '\t' + str(iold) + '  \t<->  ' + str(inew) + '\n'
                            #print mssg
                            numq[i].free = 0
                            #numq[i].dgzd = 1
                            numq[i].indx = ind
                            #print numq[i].num, numq[i].indxnumq[0].indx, '\t<->', ind + '\n'
                            #numq[i].free = 0
                            #numq[i].dgzd = 1
                            #numq[i].indx = ind
                            #print numq[i].num, numq[i].indx
                            #if i==1:
                                #numq[i].prhd='duplicate!'
                                #print '>'*i, '>', numq[i].qum, numq[i].indx, ind, numq[i].free
                                #print numq[0].num, numq[0].indx, ind, numq[0].free
                            numq[i].save()
                            #if iold = 0:
                            hdb = histo (num = str(num), type = 'indx', date = datetime.now(), xold = iold, xnew = inew)
                            print num, date, iold, inew

                            hdb.save(using = 'db')
                #else:
                    #print '-----------------------------',  num[:3]
    else:
        mssg =  'Отсортируйте абонентов по индексу, выберите групповое редактирование, скопируйте сюда весь пул номеров и нажмите Обновить'
        #
    timefinish = datetime.now()

    return render_to_response('iupd.html', {
            #'jb_filt': db, 'len' : len (db),  
            'default': mssg,
            'tim': timefinish - timestart
    })


