from django.conf.urls.defaults import patterns, include, url
from utsbase import views
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
#admin.autodiscover()
from django.conf import settings #media root

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'jinrbill.views.home', name='home'),
    # url(r'^jinrbill/', include('jinrbill.foo.urls')),
    ('^$', views.srch),
    ('^list$', views.list),
    ('^srch$', views.srch),
    #('^base/add/[0-9]+$', views.add),
    ('^edit/[0-9]+$', views.edit),
    ('^bill$', views.bill),
    ('^free$', views.free),
    ('^iupd$', views.iupd),
    ('^hstr$', views.hstr),
    #('^req$', views.req),
    #('^blank$', views.blank),
    
    (r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),

    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    #url(r'^admin/', include(admin.site.urls)),
)
